﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_UserInfo : MonoBehaviour {


    public Text _tUserId;
    public Text _tCampaignId;

    public Text _email;

    public Text _tAlias;
    public Text _tFirstName;
    public Text _tLastName;

    public Text _tAbout;

    public Text _tPatreonSince;
    public Text _tDaySincePatreon;
    public RectTransform _tIsEmailVerified;


    public InputField _tAvatarUrls;


    public Text _tGender;
    public UI_Avatars _imgAvatar;
    public UI_Avatars _imgSocialAvatars;


    public UserBean _userData;

    public void SetWithUserInfo(UserBean userData) {
        _userData = userData;


    }
}
