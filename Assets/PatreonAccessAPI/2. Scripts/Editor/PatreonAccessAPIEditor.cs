﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
[CustomEditor(typeof(PatreonAPI))]
public class PatreonAccessAPIEditor : Editor {

    private  bool _hideSensibleInformation=true;
    public override void OnInspectorGUI()
    {
        PatreonAPI myTarget = (PatreonAPI)target;

        //bool _groupCommande = 

        if(GUILayout.Button(_hideSensibleInformation ? "Show Sensitive Data" : "Hide Sensitive Data"))
        _hideSensibleInformation=!_hideSensibleInformation;

       if(!_hideSensibleInformation)
            DrawDefaultInspector();

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Debug Commands");

        if (GUILayout.Button("Generate APP ID & Secret"))
        {

            PatreonAPI.OpenBrowser_ForClientInformation();
        }
        if (GUILayout.Button("Open Authorization Page"))
        {
            PatreonAPI.OpenBrowser_ForUserAuthorization();
        }

        if (GUILayout.Button("Generate Access Token"))
        { PatreonAPI.TryGenerateToken();
        }
        if (GUILayout.Button("Refresh Access Token"))
        {
            PatreonAPI.RefreshToken();
        }
        if (GUILayout.Button("Reset Client Data"))
        {
            PatreonAPI.Reset(true, false);
        }
        if (GUILayout.Button("Reset Token Data"))
        {
            PatreonAPI.Reset(false, true);
        }


        //EditorGUILayout.LabelField("Commands:");

        //if (GUILayout.Button("Load Campaign Info"))
        //{
        //    //myTarget.LoadCampaignInformation(delegate (ref JsonCampaignData data) {
        //    //    myTarget.campaign = data;
        //    //}, false, false, false, false);
        //}
        //if (GUILayout.Button("Load Pledges Info"))
        //{
        //    //if (myTarget.campaign.data[0].id!="")
        //    //    myTarget.LoadPledgesInformation(myTarget.campaign.data[0].id, delegate (ref JsonPledgeData data) {
        //    //    myTarget.pledge = data;
        //    //});
        //}

        //if (GUILayout.Button("Load Users Info"))
        //{
        //    //myTarget.LoadCurrentUserInformation(delegate (ref z data) {
        //    //    myTarget.userInfo = data;
        //    //});

        //}
    }
}
