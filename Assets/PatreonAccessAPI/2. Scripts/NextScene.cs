﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextScene : MonoBehaviour {
    public string _defaultNextScene="";

    public void LoadNextScene()
    {
        LoadNextScene(_defaultNextScene);
    }
     public   void LoadNextScene (string nextScene) {
        if (string.IsNullOrEmpty(nextScene))
            return;
        SceneManager.LoadScene(nextScene);
	}
}
