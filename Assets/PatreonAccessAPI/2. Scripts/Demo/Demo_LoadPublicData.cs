﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo_LoadPublicData : MonoBehaviour {

    

    public InputField _userId;
    public InputField _campaignId;


    [Header("Loaded From API")]
    public UserBean _currentUserLoaded;
    public CampaignBean _currentCampaignLoaded;
    public PledgeBean _currentPledgeLoaded;

    [Header("Loaded From Web")]
    public UserBean _userLoaded;
    public CampaignBean _campaignLoaded;

    // Use this for initialization
    void Start () {
        _userId.onEndEdit.AddListener(LoadUser);
        _campaignId.onEndEdit.AddListener(LoadCampaign);
	}




    //public void LoadCurrentUserInfo()
    //{
    //    OnPublicUserLoad whenLoaded = delegate (PublicUser user)
    //    {
    //        Debug.Log("Hey Current User ;)");
    //        _userLoaded = user;
    //    };

    //    PatreonAPI.Load(whenLoaded);
    //}

    public void RefreshAll()
    {
        LoadCurrentPledge();
        LoadCurrentCampaign();
        LoadCurrentUser();
    }
    public void LoadCurrentPledge() {

    }

    public void LoadCurrentCampaign() {
        OnCompaignLoaded whenLoaded = delegate (CampaignBean campaign)
        {
            Debug.Log("Hey User ;)");
            _currentCampaignLoaded = campaign;
        };

        PatreonAPI.Load(whenLoaded);
    }
    public void LoadCurrentUser() {
        OnUserLoaded whenLoaded = delegate (UserBean user)
        {
            Debug.Log("Hey Compaign ;)");
            _currentUserLoaded = user;
        };

        PatreonAPI.Load(whenLoaded);
    }

    private void LoadCampaign(string campaingId)
    {
        OnCompaignLoaded whenLoaded = delegate (CampaignBean campaign)
        {
            Debug.Log("Hey User ;)");
            _campaignLoaded = campaign;
        };

        PatreonAPI.Load(whenLoaded, campaingId);
       
    }

    private void LoadUser(string userId)
    {
        OnUserLoaded whenLoaded = delegate (UserBean user)
        {
            Debug.Log("Hey Compaign ;)");
            _userLoaded = user;
        };

        PatreonAPI.Load(whenLoaded, userId);
    }
    
}


public struct UserInformationUI
{

    public Text _id;
    public Text _name;
    public Text _about;
    public UserBean _userData;
}
