﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CanvasToPatreonAPI : MonoBehaviour {

    public InputField _clientId;
    public InputField _clientSecret;
    public InputField _permissionCode;

    public GameObject _clientForm;
    public GameObject _permissionForm;

    public string _loginSceneToLoad ="MainScene";

    void Start() {

        DisplayInterface();
        InvokeRepeating("CheckUserLogin", 0, 0.5f);
    }

    void CheckUserLogin() {
        if (PatreonAPI.HasClientId() && PatreonAPI.HasClientSecret() &&
            PatreonAPI.HasToken() && !PatreonAPI.IsTokenExpired()) {
            if (!string.IsNullOrEmpty(_loginSceneToLoad))
            {
                LoadScene();
            }
        }
        DisplayInterface();
    }

    public  void LoadScene()
    {
        SceneManager.LoadScene(_loginSceneToLoad);
    }

    private void DisplayInterface()
    {
        
        bool needClientInformation =!PatreonAPI.HasClientId() || !PatreonAPI.HasClientSecret();
       
            _clientForm.SetActive(needClientInformation);

        bool needUserPermissionInformation = ! needClientInformation && (!PatreonAPI.HasToken() || PatreonAPI.IsTokenExpired());
       
            _permissionForm.SetActive(needUserPermissionInformation);
    }



    public void ValidateClient () {
        if (_clientId.text.Length <= 0 || _clientSecret.text.Length <= 0)
            return;
        PatreonAPI.SetClientKeys(_clientId.text, _clientSecret.text);
        DisplayInterface();
	}

    public void ValidatePermissionCode ()
    {
        if (_permissionCode.text.Length <= 0)
            return;

        PatreonAPI.SetPermissionCode(_permissionCode.text);
        PatreonAPI.TryGenerateToken();
        DisplayInterface();
    }

    public void OpenBrowserForPermission()
    {
        PatreonAPI.OpenBrowser_ForUserAuthorization();
    }
    public void OpenBrowserForClientInformation()
    {
        PatreonAPI.OpenBrowser_ForClientInformation();
    }
}
