﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_InitializePatreonAccess : MonoBehaviour {


    public string _clientId;
    public string _clientSecret;
    public string _userPermission;
    


    IEnumerator Start () {

        if (_withDebug) Debug.Log("Demo>: Load Keys information from device");
        PatreonAPI.Load();

        bool userConnected = PatreonAPI.HasToken() && !PatreonAPI.IsTokenExpired();
        if (_withDebug) Debug.Log("Demo>: Is user connected ? "+userConnected);
        while (!userConnected) {

            //bool userAlreadyLogIn = PatreonAPI.HasToken() && !PatreonAPI.IsTokenExpired();
            //bool userGavePermission = PatreonAPI.HasPermissionCode();

            if (_withDebug) Debug.Log("Demo>: Start connecting...");
            bool applicationIdDefined = false;
            do
            {
                if (_clientId.Length > 0 && _clientSecret.Length > 0)
                {
                    if (_withDebug) Debug.Log("Demo>: Set client info");
                    PatreonAPI.SetClientKeys(_clientId, _clientSecret);
                    _clientId = ""; _clientSecret = "";
                }

                applicationIdDefined = PatreonAPI.HasClientId() && PatreonAPI.HasClientSecret();
                if (!applicationIdDefined) {
                    if (_withDebug) Debug.Log("Demo>: Waitin for client info");
                    yield return new WaitForSeconds(2);

                }

            }
            while (!applicationIdDefined);

            if (_withDebug) Debug.Log("Demo>: Ask for user permission");
            PatreonAPI.OpenBrowser_ForUserAuthorization();

            while (! PatreonAPI.HasToken()) {
                if (_userPermission.Length > 0)
                {
                    if (_withDebug) Debug.Log("Demo>: Read permission");
                    PatreonAPI.SetPermissionCode(_userPermission);
                    _userPermission = "";
                    bool useDestructifAccess = true;

                    string permissionCode = PatreonAPI.GetPermissionCode(useDestructifAccess);

                    if (_withDebug) Debug.Log("Demo>: Try generateToken of " + permissionCode);
                    PatreonAPI.TryGenerateToken(permissionCode);
                }
                yield return new WaitForSeconds(1f);
            }

            if (_withDebug) Debug.Log("Demo>: Token Defined generateToken");
            bool isTokenExpired=true;
            do
            {
                isTokenExpired = (PatreonAPI.IsTokenExpired() || PatreonAPI.IsAlmostExpired());
                if (isTokenExpired)
                {
                    if (_withDebug) Debug.Log("Demo>: Try to Refresh");
                    PatreonAPI.RefreshToken();
                    yield return new WaitForSeconds(2f);
                }
                if(!isTokenExpired)
                    if (_withDebug) Debug.Log("Demo>: Token Refreshed");
            } while (isTokenExpired);

            userConnected = true;
        }

        if (PatreonAPI.HasToken() && !PatreonAPI.IsTokenExpired())
        {
            if (_withDebug) Debug.Log("Demo>: Save Info");
            PatreonAPI.Save();
            if (_withDebug) Debug.Log("Demo>: Load user Info");
            PatreonAPI.DownloadAllUserInformation();
        }

        if (_withDebug) Debug.Log("Demo>: You are Connected :)");

        while (true) { 
            yield return new WaitForSeconds(10);
            if(_withDebug)Debug.Log("Demo>: Refresh");
            PatreonAPI.RefreshToken();
            PatreonAPI.DownloadAllUserInformation();
        }

    }

    [SerializeField]
    [Header("Debug")]
    private bool _withDebug=true;
    public void Update()
    {
        if (Input.GetKey(KeyCode.Escape) && Input.GetKey(KeyCode.Delete))
            PatreonAPI.ResetToDefault();
    }
    
    
}
