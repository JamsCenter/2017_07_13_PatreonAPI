﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CampaignBean
{
    public string _id;
    public Timestamp _createdSince;
    public Timestamp _publishedSince;

    public string _creationName;
    public string _creationOneLiner;
    public int _patreonCount;
    public int _pledgeSum;
    public enum PledgeType { Video, Month}
    public PledgeType _pledgeType;

    public string _creatorId;
    public List<string> _goalsId = new List<string>();
    public List<Goal> _goals = new List<Goal>();
    public List<string> _rewardsId = new List<string>();
    public List<Reward> _rewards = new List<Reward>();

    public CampaignImage _images = new CampaignImage();
    public string _summaryMessage;
    public CampaignVideo _mainVideo = new CampaignVideo();
    public string _thanksMessage;
    public CampaignVideo _thanksVideo = new CampaignVideo();

    public string _discordServer;

    //??    //    "creation_count": 96,        
    //??        "display_patron_goals": false,
    //??        "earnings_visibility": null,
    //??        "is_charged_immediately": false,
    //??        "is_monthly": false,
    //??        "is_nsfw": false,
    //??        "is_plural": false,
    //??        "outstanding_payment_amount_cents": 0,

}

[System.Serializable]
public class Goal
{
    public string _title;
    public string _id;
    public int _amount;
    public int _amount_cent;
    public int _completedPourcentage;

    public string _description;

    public Timestamp _createAt;
    public Timestamp _reachedAt;
    public Image _image = new Image();
}

[System.Serializable]
public class Reward
{
    public string _title;
    public string _id;
    public int _amount;
    public int _amountCent;
    public string _description;

    public Timestamp _createAt;
    public Timestamp _editedAt;
    public Timestamp _publishedAt;
    public Timestamp _unpublishedAt;
    public Timestamp _deletedAt;

    public bool WasEdited() { return _editedAt.IsDefined(); }
    public bool IsStillPresent() { return _deletedAt.IsDefined(); }

    public int _patreonCount;
    public bool _isPublished;
    public Image _image = new Image();
    public CampaignVideo _video = new CampaignVideo();

    //??  "post_count": null,
    //??  "remaining": null,
    //??  "requires_shipping": false,
    //??  "user_limit": null
}

[System.Serializable]
public class Image {
    public string _imageUrl;
}

[System.Serializable]
public class CampaignVideo {
    public string _url;
    public string _youtubeEmbed;
}
[System.Serializable]
public class CampaignImage
{
    /// <summary>
    /// Small Image of the profile
    /// </summary>
    public string _campaignThrumbUrl;

    /// <summary>
    /// Image of the profile
    /// </summary>
    public string _campaignImageUrl;
}

