﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

[System.Serializable]
public class UserBean  {

    public string _id;
    public string _aliasName;
    public string _about;
    public string _firstName;
    public string _lastName;
    public string _fullName;
    public enum Gender { Male, Female}
    public Gender _gender;
    public Timestamp _onPatreonSince;
    public string _userCampaignId;

    public Social _social = new Social();
    public UserImage _images = new UserImage();
    public string _email;

    public AdditionalInfo _oAuthPlus = new AdditionalInfo();

    [System.Serializable]
    public class AdditionalInfo
    {
        public bool _isNuked;
        public bool _isEmailVerified;
        public bool _is_suspended;
        public bool _is_deleted;
        public bool _has_password;

    }

    internal string GetId()
    {
        return _id;
    }

    internal List<string> GetAvatarUrls()
    {
        List<string> urls = new List<string>();
        urls.AddRange(_social.GetAvatarUrls());
        if (_images.IsImageDefined()) {
        
                urls.Add(_images.GetPatreonImage());
        }

        if (IsMailDefined())
        {
            urls.Add(SocialUtility.Gravatar.GetProfileUrlFromId(_email,SocialUtility.SizeType.Large));
        }
        return urls;
    }
    internal bool IsMailDefined()
    {
        return ! string.IsNullOrEmpty(_email);
    }
}


[System.Serializable]
public class Social
{

    //https://www.youtube.com/JackConteMusic
    //https://www.facebook.com/JackConteMusic
    //https://twitter.com/jackconte

    public Social() {
        //get

    }
    //  Gravatar, Twitter, Twitch, Facebook, Youtube, Discord, Spotify, DeviantArt, Patreon

    public SocialUtility.SocialInfo Gravatar = 
        new SocialUtility.SocialInfo(SocialUtility.Gravatar);
    public SocialUtility.SocialInfo Twitter =
        new SocialUtility.SocialInfo(SocialUtility.Twitter);
    public SocialUtility.SocialInfo Twitch =
        new SocialUtility.SocialInfo(SocialUtility.Twitch);
    public SocialUtility.SocialInfo Facebook =
        new SocialUtility.SocialInfo(SocialUtility.Facebook);
    public SocialUtility.SocialInfo Youtube =
        new SocialUtility.SocialInfo(SocialUtility.Youtube);
    public SocialUtility.SocialInfo Discord =
        new SocialUtility.SocialInfo(SocialUtility.Discord);
    public SocialUtility.SocialInfo Spotify =
        new SocialUtility.SocialInfo(SocialUtility.Spotify);
    public SocialUtility.SocialInfo DeviantArt =
        new SocialUtility.SocialInfo(SocialUtility.Deviantart);
    public SocialUtility.SocialInfo Patreon =
        new SocialUtility.SocialInfo(SocialUtility.Patreon);

    internal List<string> GetAvatarUrls(SocialUtility.SizeType size = SocialUtility.SizeType.Large)
    {
        List<string> urls = new List<string>();
        GetImageIfValide(size, ref urls, ref Gravatar);
        GetImageIfValide(size, ref urls, ref Twitter);
        GetImageIfValide(size, ref urls, ref Twitch);
        GetImageIfValide(size, ref urls, ref Facebook);
        GetImageIfValide(size, ref urls, ref Youtube);
        GetImageIfValide(size, ref urls, ref Discord);
        GetImageIfValide(size, ref urls, ref Spotify);
        GetImageIfValide(size, ref urls, ref DeviantArt);
        GetImageIfValide(size, ref urls, ref Patreon);
        return urls;
    }

    private void GetImageIfValide(SocialUtility.SizeType size,ref  List<string> urls, ref SocialUtility.SocialInfo info)
    {
        string url = info.GetImageUrl(size);
        if (!string.IsNullOrEmpty(url))
            urls.Add(url);
    }
}
[System.Serializable]
public class UserImage
{
    /// <summary>
    /// Small Image of the profile
    /// </summary>
    [SerializeField]
    private string _profileThrumbUrl;

    /// <summary>
    /// Image of the profile
    /// </summary>
    [SerializeField]
    private string _profileImageUrl;

    public enum PatreonImage { Default, Thrumb}
    public string GetPatreonImage(PatreonImage imageType=PatreonImage.Default) {
        if (imageType == PatreonImage.Default)
            return _profileImageUrl;
        return _profileThrumbUrl;
    }

    public bool IsImageDefined(string url)
    {
        //Is null no image
        if (string.IsNullOrEmpty(url))
            return false;
        //If regex of a default photo
        bool isDefaultPhoto = true;
        isDefaultPhoto = IsStandardImage(url);

        if (isDefaultPhoto)
            return false;
        return true;

    }

    private static bool IsStandardImage(string url)
    {
        return SocialUtility.PatreonAccess.IsDefaultImage(url);
    }

    public bool IsImageDefined()
    {
        return IsImageDefined(_profileImageUrl);
    }

    internal void SetPatreonImageTo(string imageUrl, string thumbUrl)
    {
        _profileImageUrl = imageUrl;
        _profileThrumbUrl = thumbUrl;
    }

    public bool IsThrumbDefined()
    {
        return IsImageDefined(_profileThrumbUrl);
    }

}
