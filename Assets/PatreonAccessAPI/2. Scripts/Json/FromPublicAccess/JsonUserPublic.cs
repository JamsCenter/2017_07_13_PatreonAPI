﻿

using System.Collections.Generic;

namespace PatreonUnityAPI.Public.User
{
    [System.Serializable]
    public class UserAttributes
    {
        //PUBLIC
        public string about;
        public string created ;
        public string facebook ;
        public string first_name ;
        public string full_name ;
        public int gender ;
        public string image_url ;
        public string last_name ;
        public string thumb_url ;
        public string twitch ;
        public string twitter ;
        public string url ;
        public string vanity ;
        public string youtube ;

        //PRIVATE
        public string discord_id;
        public string email;
        public string facebook_id;
        public bool has_password;
        public bool is_email_verified;
        public bool is_nuked;
        public bool is_suspended;
        public bool is_deleted;

        public PrivateSocialConnection social_connections=new PrivateSocialConnection();
    }
    [System.Serializable]
    public class PrivateSocialConnection {

        [System.Serializable]
        public class SocialId { public  string user_id; }
        public SocialId facebook = new SocialId();
        public SocialId deviantart = new SocialId();
        public SocialId spotify = new SocialId();
        public SocialId twitch = new SocialId();
        public SocialId twitter = new SocialId();
        public SocialId youtube = new SocialId();
        public SocialId discord = new SocialId();

    }

    [System.Serializable]
    public class CompaignData
    {
        public string id;
        public string type;
    }

    [System.Serializable]
    public class CompaignLink
    {
        public string related;
    }

    [System.Serializable]
    public class Campaign
    {
        public CompaignData data;
        public CompaignLink links;
    }

    [System.Serializable]
    public class Relationships
    {
        public Campaign campaign;
    }

    [System.Serializable]
    public class JsonPublicData_User
    {
        public UserAttributes attributes;
        public string id;
        public Relationships relationships;
        public string type;
    }
    
    [System.Serializable]
    public class JsonRootUserInfo
    {
        public JsonPublicData_User data= new JsonPublicData_User();
    }
}