﻿using System.Collections.Generic;

namespace PatreonUnityAPI.Public.Campaign
{

    [System.Serializable]
    public class Attributes

    {
        //PUBLIC
        public string created_at ;
        public int creation_count ;
        public string creation_name ;
        public bool display_patron_goals ;
        public object earnings_visibility ;
        public string image_small_url ;
        public string image_url ;
        public bool is_charged_immediately ;
        public bool is_monthly ;
        public bool is_nsfw ;
        public bool is_plural ;
        public string main_video_embed ;
        public string main_video_url ;
        public string one_liner ;
        public int patron_count ;
        public string pay_per_name ; // Month / Video
        public int pledge_sum ;
        public string pledge_url ;
        public string published_at ;
        public string summary;

        //PRIVATE 
        public string discord_server_id;
        public int outstanding_payment_amount_cents;
        public string thanks_embed;
        public string thanks_msg;
        public string thanks_video_url;

    }

    [System.Serializable]
    public class Data2
    {
        public string id ;
        public string type ;
    }

    [System.Serializable]
    public class Links
    {
        public string related ;
    }

    [System.Serializable]
    public class Creator
    {
        public Data2 data ;
        public Links links ;
    }

    [System.Serializable]
    public class Datum
    {
        public string id ;
        public string type ;
    }

    [System.Serializable]
    public class Goals
    {
        public List<Datum> data ;
    }

    [System.Serializable]
    public class Datum2
    {
        public string id ;
        public string type ;
    }

    [System.Serializable]
    public class Rewards
    {
        public List<Datum2> data ;
    }

    [System.Serializable]
    public class Relationships
    {
        public Creator creator ;
        public Goals goals ;
        public Rewards rewards ;
    }

    [System.Serializable]
    public class JsonCompaignInfo
    {
        public Attributes attributes ;
        public string id ;
        public Relationships relationships ;
        public string type ;
    }

    [System.Serializable]
    public class Attributes2
    {
        public string about ;
        public string created ;
        public string facebook ;
        public string first_name ;
        public string full_name ;
        public int gender ;
        public string image_url ;
        public string last_name ;
        public string thumb_url ;
        public string twitch ;
        public string twitter ;
        public string url ;
        public string vanity ;
        public string youtube ;
        public string created_at ;
        public string description;
        public string type;
        public string deleted_at ;
        public string discord_role_ids ;
        public string edited_at;
        public string published_at ;
        public string title ;
        public string unpublished_at;
        public string reached_at;
        //Not in all attributes
        public int amount;
        public int amount_cents;
        public int remaining;
        public bool requires_shipping;
        public int user_limit;
        public int patron_count;
        public int post_count;
        public bool published;
        public int completed_percentage;
    }

    [System.Serializable]
    public class Data3
    {
        public string id ;
        public string type ;
    }

    [System.Serializable]
    public class Links2
    {
        public string related ;
    }

    [System.Serializable]
    public class Campaign
    {
        public Data3 data ;
        public Links2 links ;
    }

    [System.Serializable]
    public class Data4
    {
        public string id ;
        public string type ;
    }

    [System.Serializable]
    public class Links3
    {
        public string related ;
    }

    [System.Serializable]
    public class Creator2
    {
        public Data4 data ;
        public Links3 links ;
    }

    [System.Serializable]
    public class Relationships2
    {
        public Campaign campaign ;
        public Creator2 creator ;
    }

    [System.Serializable]
    public class JsonCompaignIncludedInfo
    {
        public Attributes2 attributes ;
        public string id ;
        public Relationships2 relationships ;
        public string type ;
    }

    [System.Serializable]
    public class Links4
    {
        public string self ;
    }

    [System.Serializable]
    public class J_PublicCampaignInfo
    {
        public JsonCompaignInfo data ;
        public List<JsonCompaignIncludedInfo> included ;
        public Links4 links ;
    }
    public class J_PrivateCampaignInfo {

        public List<JsonCompaignInfo> data;
        public List<JsonCompaignIncludedInfo> included;

    }
}