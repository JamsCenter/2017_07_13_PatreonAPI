﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public class CoroutineManager : MonoBehaviour
{

    public int _maxThread = 20;
    public Queue<CoroutineRef> WaitingCoroutines = new Queue<CoroutineRef>();
    public List<CoroutineRef> RunningCoroutines = new List<CoroutineRef>();

    public List<CoroutineRef> LoopingCoroutines = new List<CoroutineRef>();
    public Dictionary<long, CoroutineRef> StoredCoroutines = new Dictionary<long, CoroutineRef>();

   


    #region GETTER
    public int WaitingCount { get { return WaitingCoroutines.Count; } }
    public int RunningCount { get { return RunningCoroutines.Count; } }
    public int MaxThreads { get { return _maxThread; } }
    #endregion

    #region SETTER
    public void SetMaxThreadTo(int max) { _maxThread = max; }
    #endregion

    
    [Serializable]
    public class CoroutineRef
    {

        private static long idIterator;
        [SerializeField]
        private long _id;
        public long Id { get { return _id; } }

        public CoroutineRef(IEnumerator inWaiting) {
            _inWaitingCoroutine = inWaiting;
            _runningCoroutine = null;
            _hasStarted = false;
            _id = idIterator++;

        }
        private IEnumerator _inWaitingCoroutine;
        public IEnumerator InWaiting { get { return _inWaitingCoroutine; } }
        
        private bool _hasStarted;
        public bool HasBeenStart() { return _hasStarted; }
        
        public Coroutine _runningCoroutine;
        public bool IsFinished() {

            //Debug.Log("C: "+ _hasStarted + " - "+ _inWaitingCoroutine.Current+" - "+ _runningCoroutine);
            //Debug.Log("C:> " + _runningCoroutine.ToString());
            if (_runningCoroutine == null)
                return false;
            if (_inWaitingCoroutine == null)
                return false;
            if (!_hasStarted)
                return false;

            //            _inWaitingCoroutine.
            if (_inWaitingCoroutine.Current != null) { 
                Type t = _inWaitingCoroutine.Current.GetType();
                if (t == typeof(WWW))
                {
                    WWW w = (WWW)_inWaitingCoroutine.Current;
                 //   Debug.Log("C: " + w.isDone);

                    return w.isDone;
                }
                //else if (t == typeof(WaitForSeconds))
                //{
                //    WaitForSeconds w = (WaitForSeconds)_inWaitingCoroutine.Current;
                //    Debug.Log("C: " + w.);

                //    return w.isDone;
                //}
            }

         //   Debug.Log("C: " + _inWaitingCoroutine.Current);
            return _inWaitingCoroutine.Current==null;
            //return false;
        }

        public void SetAsStarted(Coroutine coroutine) {
            if (_hasStarted)
                return;
            _hasStarted = true;
            _runningCoroutine = coroutine;

        }
    }

    public static long StartCoroutineAsap(IEnumerator toLaunchWantReady)
    {
        CoroutineRef inWaiting = new CoroutineRef(toLaunchWantReady);
        Instance.WaitingCoroutines.Enqueue(inWaiting);
        return inWaiting.Id;
    }


    public static long StartEndlessCoroutine(IEnumerator toLaunchWantReady)
    {

        CoroutineRef inWaiting = new CoroutineRef(toLaunchWantReady);
        Instance.LoopingCoroutines.Add(inWaiting);
        StartRefCoroutine(inWaiting);

        return inWaiting.Id;
    }

    private static void StartRefCoroutine(CoroutineRef inWaiting)
    {
        Coroutine coroutine = Instance.StartCoroutine(inWaiting.InWaiting);
        inWaiting.SetAsStarted(coroutine);
    }

    public void AddCoroutine(long id, CoroutineRef coroutine) {
        StoredCoroutines.Add(id, coroutine);
    }
    public void RemoveCoroutine(long id) {
        if (StoredCoroutines.ContainsKey(id))
            StoredCoroutines.Remove(id);
    }



    public IEnumerator Start() {
        
        while (true) {

            while (RunningCount < MaxThreads && WaitingCount>0) {
                CoroutineRef inWaiting = WaitingCoroutines.Dequeue();
                RunningCoroutines.Add(inWaiting);
                StartRefCoroutine(inWaiting);
            }

            CoroutineRef[] running = RunningCoroutines.ToArray();
            for (int i = 0; i < running.Length; i++)
            {
                if (running[i].IsFinished())
                {
                    RunningCoroutines.Remove(running[i]);
                }

            }

            CoroutineRef[] loop = LoopingCoroutines.ToArray();
            for (int i = 0; i < loop.Length; i++)
            {
                if (loop[i].IsFinished())
                {
                    LoopingCoroutines.Remove(loop[i]);
                    StoredCoroutines.Remove(loop[i].Id);
                }

            }

            _inWaiting = WaitingCoroutines.Count;
            _inRunning = RunningCoroutines.Count;
            _inLooping = LoopingCoroutines.Count;

            yield return new WaitForSeconds(0.5f);
        }
    }

   

    [Header("Debug")]
    public int _inWaiting;
    public int _inRunning;
    public int _inLooping;

    #region SINGLETON ACCESS 
    private static CoroutineManager _instanceInScene;
    public static CoroutineManager Instance
    {
        get
        {   //////// Return Instance :) ///////
            bool hasInstance = _instanceInScene != null;
            if (hasInstance)
                return _instanceInScene;
            //////// Find the instance in scene /////
            _instanceInScene = FindObjectOfType<CoroutineManager>();

            hasInstance = _instanceInScene != null;
            if (hasInstance)
                return _instanceInScene;

            ///////// Create the instance in the scene////////////////
            GameObject createdLoader = new GameObject("# Coroutine Manager");
            _instanceInScene = createdLoader.AddComponent<CoroutineManager>();

            return _instanceInScene;
        }
        private set
        {
            if (_instanceInScene == null)
                _instanceInScene = value;
        }
    }
    public void Awake()
    {
        Instance = this;
    }
    #endregion

}