﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PatreonToken
{
    public PatreonToken(string accessToken, string refreshToken, int lifeTimeInSecond, string authorizationScope, string accessType)
    {
        SetData( accessToken, refreshToken, lifeTimeInSecond, authorizationScope, accessType);
    }

    public PatreonToken()
    {
        SetData("","",0,"","");

    }

    public void SetData(string accessToken, string refreshToken, int lifeTimeInSecond, string authorizationScope, string accessType)
    {
        _accessToken = accessToken;
        _refreshToken = refreshToken;
        _lifeTime = lifeTimeInSecond;
        int now = NowInSecond();
        _expirationTimetamp = now +_lifeTime;
        _accessType = accessType;
        _authorizationScope = authorizationScope;
    }
    public int NowInSecond() { return (int)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds; }
    [SerializeField]
    private string _accessToken;
    public string Token { get { return _accessToken; } }

    [SerializeField]
    private string _refreshToken;
    public string Refresh{get{return _refreshToken;}}

    [SerializeField]
    private int _lifeTime;
    [SerializeField]
    private int _expirationTimetamp;
    public int TimeLeft { get { return _expirationTimetamp-NowInSecond(); } }

    [SerializeField]
    private string _authorizationScope;

    [SerializeField]
    private string _accessType;


    public void Reset() {
        SetData("", "", 0, "", "");
     }
}

[Serializable]
public class PermissionCode {
    [SerializeField]
    string _code;
    public string GetCode(bool destructiveAccess) {
        string code = _code;
        if (destructiveAccess)
            _code = "";
        return code;
    }

    public bool HasCode() { return !string.IsNullOrEmpty(_code); }

    public void SetCode(string code)
    {
        _code = code;
    }

    internal void Reset()
    {
        _code = "";
    }
}

[Serializable]
public struct OAuth
{
    public OAuth(string redirect, string scope, string state)
    {
        _redirectURI = redirect;
        _scope = scope;
        _state = state;
    }
    [SerializeField]
    private string _redirectURI;
    [SerializeField]
    private string _scope;
    [SerializeField]
    private string _state;

    public string Redirection { get { return _redirectURI; } }
    public string Scope { get { return _scope; } }
    public string State { get { return _state; } }
}