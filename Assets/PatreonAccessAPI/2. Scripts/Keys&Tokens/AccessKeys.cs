﻿using System;
using UnityEngine;

[Serializable]
public class AccessKeys
{

    public AccessKeys(string id, string secret, string token)
    {
        SetKeys(id, secret, token);
    }

    public AccessKeys()
    {
        SetKeys("", "", "");
    }
    internal void SetKeys(string clientId, string clientSecret)
    {
        _clientId = clientId;
        _clientSecret = clientSecret;
    }
    public void SetKeys(string id, string secret, string token)
    {
        _clientId = id;
        _clientSecret = secret;
        _userTokenAccess = token;
    }

    internal void SetToken(string token)
    {
        _userTokenAccess = token;
    }

    [SerializeField]
    string _clientId;
    public string ClientId { get { return _clientId; } }
    [SerializeField]
    string _clientSecret;
    public string ClientSecret { get { return _clientSecret; } }
    [SerializeField]
    string _userTokenAccess;
    public string Token { get { return _userTokenAccess; } }

    public bool IsKeysDefined() { return string.IsNullOrEmpty(_clientId) && string.IsNullOrEmpty(_clientSecret) && string.IsNullOrEmpty(_userTokenAccess); }
    public bool IsAppKeysDefined() { return string.IsNullOrEmpty(_clientId) && string.IsNullOrEmpty(_clientSecret); }
    public bool IsTokenKeyDefined() { return string.IsNullOrEmpty(_userTokenAccess); }

    internal void Reset()
    {
        SetKeys("", "", "");
    }

}


public class NotAccessKeyDefined : Exception
{
    public NotAccessKeyDefined(string message) : base(message) { }
}
