﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;

public class PatreonUtility  {


    public static long GetTimeStampFor(string stringDate) {
        return DateTimeToTimestamp( GetDateFor(stringDate) );
    }
    public static DateTime GetDateFor(string stringDate) {
        if (string.IsNullOrEmpty(stringDate))
            throw new System.ArgumentNullException();
        //string format = "yyyy - dd - MMT00: hh:mm + 00:00";
        //CultureInfo provider = CultureInfo.InvariantCulture;
        //return DateTime.ParseExact(stringDate , format , provider);
        return DateTime.Parse(stringDate);
    }

    public static long Timestamp() { return DateTimeToTimestamp(DateTime.UtcNow); }
    public static long DateTimeToTimestamp(DateTime timestamp) {
        return (timestamp.Ticks - _1970.Ticks) / 10000000;
    }
    public static DateTime TimestampToDateTime(long timestamp) {
        return _1970.AddSeconds(timestamp);
    }

    public static DateTime _1970 { get { return new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc); } }  

}
[System.Serializable]
public class Timestamp
{
    public long _value;
    public long Value { get { return _value; } private set { _value = value; } }
    public bool IsDefined() { return Value == 0; }
    public Timestamp(string stringDate) {
        if (string.IsNullOrEmpty(stringDate))
            Value = 0;
        else
            Value = PatreonUtility.GetTimeStampFor(stringDate);
    }
    public Timestamp(DateTime date) { Value = PatreonUtility.DateTimeToTimestamp(date); }

    public static Timestamp Create(string stringDate)
    {
        return new Timestamp(stringDate);
    }
    public static Timestamp Create(DateTime date)
    {
        return new Timestamp(date);
    }
}
