<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en"> 
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<title>Patreon Access</title>
		<style>
		html, body {
  margin: 0;
  overflow: hidden;
}

.wrapper {
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
}

.color-bg-start {
  background-color: salmon;
}

.bg-animate-color {
  animation: random-bg .5s linear infinite;
}

@keyframes random-bg {
  from {
    filter: hue-rotate(0);
  }
  to {
    filter: hue-rotate(360deg);
  }
}

.fun-btn {
  background-color: tomato;
  color: white;
  padding: 2em 3em;
  border: none;
  transition: all .3s ease;
  border-radius: 5px;
  letter-spacing: 2px;
  text-transform: uppercase;
  outline: none;
  align-self: center;
  cursor: pointer;
  font-weight: bold;
  font-size: 20px;
}



@keyframes grow {
  0% { transform: scale(1); }
  14% { transform: scale(1.3); }
  28% { transform: scale(1); }
  42% { transform: scale(1.3); }
  70% { transform: scale(1); }
}
html, body {
  margin: 0;
  background: #43cea2;
  background: -webkit-linear-gradient(to left, #43cea2 , #185a9d);
  background: linear-gradient(to left, #43cea2 , #185a9d);  
}




textarea {
    resize: none;
	text-align:center;
	 white-space: normal;
    text-align: justify;
    -moz-text-align-last: center; /* Firefox 12+ */
    text-align-last: center;
}

    footer{left:35%; right:35%; bottom: 12px; position: fixed; z-index: 1; border-radius: 50px;  border-style: solid;
    border-color: #ffffff;
    border-width: 1px;}
    footer .site-info {padding:5px 5px 5px; text-align:center;border:50px; color:white; }
    footer .site-info a { color:white;
 text-decoration:none;   }
</style>
		<!--<?php  
		echo '<h2>Scope:</h2><p>'. $_GET["scope"] ."</p>";
		echo '<h2>Error:</h2><p>'. $_GET["error"] ."</p>";
		?>-->
	</head>
	<body>
		<div class="wrapper">
		  <textarea class="fun-btn" rows="1" cols="40" style="overflow:hidden"
		  <?php 
		  if(!isset($_GET["code"])) 
			  echo 'onClick="location.href=\'https://github.com/JamsCenter/2017_07_13_PatreonUnityAPI/wiki\'"';
		  ?>><?php 
		  if(isset($_GET["code"]))
			  echo $_GET["code"];
		  else if (isset($_GET["error"]))
			  echo $_GET["error"];
		  else echo 'RTFM';

		  ?></textarea>
		</div>
		
		
		
	<footer style="background-color:rgba(0, 0, 0, 0.9);background-border-color:rgba(1, 1, 1, 1);">
        <div class="site-info">
            <div>
              <a href="https://github.com/JamsCenter/2017_07_13_PatreonUnityAPI/wiki">Manual</a> | <a href="http://patreon.com/2AM">Support</a> | <a href="http://jams.center"> Contact</a>
			 </div>
        </div>    
    </footer>
	</body>
</html>
