﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo_LoadAuthorizedData : MonoBehaviour {


    public Text _userInfo;
    public Text _userCampaignInfo;
    public Text _userPledgeInfo;
    public UserBean _user;
    public CampaignBean _campaign;
    public PledgeBean _pledge;


    public void Start()
    {
        LoadUser();
        LoadCampaign();
       // LoadPledge();
    }
    private void LoadUser()
    {
        OnUserLoaded whenLoaded = delegate (UserBean user)
        {
            Debug.Log("Hey mon ami ;)");
            if (_userInfo)
                _userInfo.text = user._aliasName;
            _user = user;
        };

        PatreonAPI.Load(whenLoaded);
    }
    private void LoadCampaign()
    {
        OnCompaignLoaded whenLoaded = delegate (CampaignBean campaign)
        {
            Debug.Log("Hey mon ami ;)");
            if (_userCampaignInfo)
                _userCampaignInfo.text = campaign._creatorId;
            _campaign = campaign;
        };

        PatreonAPI.Load(whenLoaded);
    }
    //private void LoadPledge()
    //{
    //    OnPledgeLoaded whenLoaded = delegate (PledgeBean pledge)
    //    {
    //        Debug.Log("Hey mon ami ;)");
    //        if (_pledge)
    //            _pledge.text = pledge._toDo;
    //          _pledge = pledge;
    //    };

    //    PatreonAPI.Load(whenLoaded);
    //}


}
